package ru.iteco.vetoshnikov.taskmanager.endpoint;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.iteco.vetoshnikov.taskmanager.api.IProjectEndpoint;
import ru.iteco.vetoshnikov.taskmanager.api.IProjectService;
import ru.iteco.vetoshnikov.taskmanager.dto.ProjectDTO;

import java.util.List;

@Getter
@Setter
@Component
public class ProjectEndpoint implements IProjectEndpoint {
    @Autowired
    IProjectService projectService;

    @Override
    public List<ProjectDTO> listProjects() {
        return projectService.allProjects();
    }

    @Override
    public void saveProject(
            @Nullable final ProjectDTO projectDTO
    ) {
        if (projectDTO == null) return;
        projectService.add(projectDTO);
    }

    @Override
    public void deleteProject(
            @Nullable final ProjectDTO projectDTO
    ) {
        if (projectDTO == null) return;
        projectService.delete(projectDTO);
    }

    @Override
    public void editProject(
            @Nullable final ProjectDTO projectDTO
    ) {
        if (projectDTO == null) return;
        projectService.edit(projectDTO);
    }

    @Override
    public ProjectDTO getByIdProject(
            @Nullable final String id
    ) {
        if (id == null || id.isEmpty()) return null;
        return projectService.getById(id);
    }
}

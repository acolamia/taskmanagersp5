package ru.iteco.vetoshnikov.taskmanager.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.iteco.vetoshnikov.taskmanager.constant.StatusType;
import ru.iteco.vetoshnikov.taskmanager.dto.ProjectDTO;
import ru.iteco.vetoshnikov.taskmanager.api.IProjectService;

import javax.faces.bean.RequestScoped;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
@RequestScoped
public class ProjectController {
    @Autowired
    private IProjectService projectService;

    private final Map<StatusType, String> statusTypeMap = StatusType.getVALUES();

    private List<ProjectDTO> projectDTOList = new ArrayList<>();

    private ProjectDTO tempProjectDTO = new ProjectDTO();

    private String id;

    public void createEmptyProject() {
        projectService.add(new ProjectDTO());
    }

    public void delete(@Nullable final String id) {
        if (id == null || id.isEmpty()) return;
        @NotNull final ProjectDTO projectDTO = projectService.getById(id);
        projectService.delete(projectDTO);
    }

    public String edit() {
        projectService.edit(tempProjectDTO);
        id = null;
        tempProjectDTO = null;
        return "projectListJsf?faces-redirect=true";
    }

    //GETTERS and SETTERS :

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Map<StatusType, String> getStatusTypeMap() {
        return statusTypeMap;
    }

    public List<ProjectDTO> getProjectDTOList() {
        return projectDTOList = projectService.allProjects();
    }

    public void setTempProjectDTO() {
        if (id == null || id.isEmpty()) return;
        this.tempProjectDTO = projectService.getById(id);
    }

    public ProjectDTO getTempProjectDTO() {
        return tempProjectDTO;
    }

    //GETTERS and SETTERS.
}

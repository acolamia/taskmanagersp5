package ru.iteco.vetoshnikov.taskmanager.api;

import ru.iteco.vetoshnikov.taskmanager.dto.TaskDTO;

import java.util.List;

public interface ITaskService {
    List<TaskDTO> allTasks(String projectId);

    void add(TaskDTO taskDTO);

    void delete(String taskId);

    void deleteAllByProjectId(String projectId);

    void edit(TaskDTO taskDTO);

    TaskDTO getByProjectIdAndId(String projectId, String id);

    TaskDTO getById(String id);
}

package ru.iteco.vetoshnikov.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.iteco.vetoshnikov.taskmanager.api.ITaskService;
import ru.iteco.vetoshnikov.taskmanager.dto.TaskDTO;
import ru.iteco.vetoshnikov.taskmanager.model.Task;
import ru.iteco.vetoshnikov.taskmanager.repository.TaskRepository;
import ru.iteco.vetoshnikov.taskmanager.util.ConvertDTOUtil;

import java.util.List;

@Service
@Transactional
public class TaskService implements ITaskService {
    @NotNull
    @Autowired
    TaskRepository taskRepository;

    @Nullable
    @Override
    public List<TaskDTO> allTasks(@Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) return null;
        return ConvertDTOUtil.convertTaskToDTOList(taskRepository.findAllByProjectId(projectId));
    }

    @Override
    public void add(@Nullable final TaskDTO taskDTO) {
        if (taskDTO == null) return;
        @NotNull final Task task=ConvertDTOUtil.convertDTOToTask(taskDTO);
        taskRepository.save(task);
    }

    @Override
    public void delete(@Nullable final String taskId) {
        if (taskId == null) return;
        taskRepository.deleteById(taskId);
    }

    @Override
    public void deleteAllByProjectId(@Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) return;
        taskRepository.deleteAllByProjectId(projectId);
    }

    @Override
    public void edit(@Nullable final TaskDTO taskDTO) {
        if (taskDTO == null) return;
        @NotNull final Task task=ConvertDTOUtil.convertDTOToTask(taskDTO);
        taskRepository.save(task);
    }

    @Nullable
    @Override
    public TaskDTO getByProjectIdAndId(@Nullable final String projectId, String id) {
        if (projectId == null || projectId.isEmpty() || id == null || id.isEmpty()) return null;
        return ConvertDTOUtil.convertTaskToDTO(taskRepository.getByProjectIdAndId(projectId, id));
    }

    @Nullable
    @Override
    public TaskDTO getById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        return ConvertDTOUtil.convertTaskToDTO(taskRepository.getOne(id));
    }
}

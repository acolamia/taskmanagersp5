package ru.iteco.vetoshnikov.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.iteco.vetoshnikov.taskmanager.api.IProjectService;
import ru.iteco.vetoshnikov.taskmanager.dto.ProjectDTO;
import ru.iteco.vetoshnikov.taskmanager.model.Project;
import ru.iteco.vetoshnikov.taskmanager.repository.ProjectRepository;
import ru.iteco.vetoshnikov.taskmanager.util.ConvertDTOUtil;

import java.util.List;

@Service
@Transactional
public class ProjectService implements IProjectService {
    @NotNull
    @Autowired
    ProjectRepository projectRepository;

    @Nullable
    @Override
    public List<ProjectDTO> allProjects() {
        return ConvertDTOUtil.convertProjectToDTOList(projectRepository.findAll());
    }

    @Override
    public void add(@Nullable final ProjectDTO projectDTO) {
        if (projectDTO == null) return;
        @NotNull final Project project = ConvertDTOUtil.convertDTOToProject(projectDTO);
        projectRepository.save(project);
    }

    @Override
    public void delete(@Nullable final ProjectDTO projectDTO) {
        if (projectDTO == null) return;
        @NotNull final Project project = projectRepository.getOne(projectDTO.getId());
        projectRepository.delete(project);
    }

    @Override
    public void edit(@Nullable final ProjectDTO projectDTO) {
        if (projectDTO == null) return;
        @NotNull final Project project = ConvertDTOUtil.convertDTOToProject(projectDTO);
        projectRepository.save(project);
    }

    @Nullable
    @Override
    public ProjectDTO getById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        return ConvertDTOUtil.convertProjectToDTO(projectRepository.getOne(id));
    }

    public void CREATEPROJECT(){
        projectRepository.save(new Project());
    }
}
